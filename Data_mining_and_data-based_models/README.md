# Exercise 1 -- From TEXT to CONTEXT
## Mine text and context for
1. HER2 gene in cancer
2. APC gene in cancer

# Exercise 2 -- Repositories and databases
## Identify molecular subgroups and perform analyses for:
1. Mutated BRCA1 gene vs amplified ERBB2 in breast cancer (BRCA) using cBioPortal
2. Mutated PIM1 gene vs amplified REL in B cell lymphoma (DLBCL / DLBC / DLBL) using cBioPortal
3. Mutated FLT3 gene vs amplified ERG in acute myeloid leukemia (AML) using cBioPortal
4. Mutated KRAS gene vs amplified TERT in lung adenocarcinoma (LUAD) using cBioPortal

# Exercise 3 -- From genes to pathways and networks
## Use DEGs to perform pathway and network analyses for:
1. Mutated BRCA1 gene vs amplified ERBB2 in breast cancer (BRCA) using cBioPortal
2. Mutated MUC4 gene vs amplified MYC in breast cancer (BRCA) using cBioPortal
3. With networks from STRING and pathways from REACTOME:
   * Compare enrichments and networks
   * Focus on the more connected genes (hubs) and expand your knowledge on them, trying to identify their molecular context and how do they impact on this tumor

## Use DEGs to perform pathway and network analyses for:
1. Mutated KRAS gene vs amplified TERT in lung adenocarcinoma (LUAD) using cBioPortal
2. Mutated MUC16 gene vs amplified SOX2 in squamous lung carcinoma (LUSC) using cBioPortal
3. With networks from STRING and pathways from REACTOME:
   * Compare enrichments and networks
   * Focus on the more connected genes (hubs) and expand your knowledge on them, trying to identify their molecular context and how do they impact on this tumor

# Exercise 4 -- Of single cells and clusters
## Fibroblasts (mesenchymal cells) - The Human Protein Atlas – Single Cell Type
1. Input these genes into reactome, string etc. and identify pathways and networks involved
2. Single cell browser -- input the top 100 genes from the step above, identifying the cell types mostly involved (Select the study *"A single-cell and spatially resolved atlas of human breast cancers"*)
3. Compare the results and, helping yourself with the literature, make sense of what cell types are mostly expressing these genes, why, what pathway they might represent etc.

## Macrophages (blood & immune cells) - The Human Protein Atlas – Single Cell Type
1. Input these genes into reactome, string etc. and identify pathways and networks involved
2. Single cell browser -- input the top 100 genes from the step above, identifying the cell types mostly involved (Select the study *"A single-cell and spatially resolved atlas of human breast cancers"*)
3. Compare the results and, helping yourself with the literature, make sense of what cell types are mostly expressing these genes, why, what pathway they might represent etc.

# Exercise 5 -- Clinical data and OMICs
## Identify molecular subgroups and perform analyses for:
1. Mutated BRCA1 gene vs amplified ERBB2 in breast cancer (BRCA) using cBioPortal
2. Check which differentially-expressed genes are involved in survival (cBioPortal and/or Xena)

## Identify molecular subgroups and perform analyses for:
1. Mutated KRAS gene vs amplified TERT in lung adenocarcinoma (LUAD) using cBioPortal
2. Check which differentially-expressed genes are involved in survival (cBioPortal and/or Xena)
