# Mutated FLT3 gene vs amplified ERG in acute myeloid leukemia (AML) -- cBioPortal

## OncoPrint
* altered/profiled:
  - FLT3: 1.9%
  - ERG: 5%

## Cancer Types Summary

| **FLT3** |
|:-:|
| ![FLT3](images/cancer_types_summary-FLT3.png) |

| **ERG** |
|:-:|
| ![ERG](images/cancer_types_summary-ERG.png) |

## Plots
* Copy number:

| **FLT3** |
|:-:|
![FLT3](images/copy_number-alterations-FLT3.png)

| **ERG** |
|:-:|
![ERG](images/copy_number-alterations-ERG.png)

* Mutations:

| **FLT3** |
|:-:|
![FLT3](images/mutations_FLT3.png)

| **ERG** |
|:-:|
![ERG](images/mutations_ERG.png)

## Conclusion
FLT3 mutations (missense) and ERG amplification are implicated in B cell lymphoma.