# Mutated BRCA1 gene vs amplified ERBB2 in breast cancer (BRCA) -- cBioPortal

## OncoPrint
* altered/profiled:
  - BRCA1: 3%
  - ERBB2: 14%

## Cancer Types Summary

| **BRCA1** |
|:-:|
| ![BRCA1](images/cancer_types_summary-BRCA1.png) |

| **ERBB2** |
|:-:|
| ![ERBB2](images/cancer_types_summary-ERBB2.png) |

## Plots
* Copy number:

| **BRCA1** |
|:-:|
![BRCA1](images/copy_number-alterations-BRCA1.png)

| **ERBB2** |
|:-:|
![ERBB2](images/copy_number-alterations-ERBB2.png)

* Mutations:

| **BRCA1** |
|:-:|
![BRCA1](images/mutations_BRCA1.png)

| **ERBB2** |
|:-:|
![ERBB2](images/mutations_ERBB2.png)

## Conclusion
BRCA1 mutations (missense) and ERBB2 amplification are implicated  in breast cancer.