# Mutated KRAS gene vs amplified TERT in lung adenocarcinoma (LUAD) -- cBioPortal

## OncoPrint
* altered/profiled:
  - KRAS: 28%
  - TERT: 11%

## Cancer Types Summary

| **KRAS** |
|:-:|
| ![KRAS](images/cancer_types_summary-KRAS.png) |

| **TERT** |
|:-:|
| ![TERT](images/cancer_types_summary-TERT.png) |

## Plots
* Copy number:

| **KRAS** |
|:-:|
![KRAS](images/copy_number-alterations-KRAS.png)

| **TERT** |
|:-:|
![TERT](images/copy_number-alterations-TERT.png)

* Mutations:

| **KRAS** |
|:-:|
![KRAS](images/mutations_KRAS.png)

| **TERT** |
|:-:|
![TERT](images/mutations_TERT.png)

## Conclusion
KRAS mutations (missense) and TERT amplification are implicated in B cell lymphoma.