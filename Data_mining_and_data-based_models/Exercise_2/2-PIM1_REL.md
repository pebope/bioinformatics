# Mutated PIM1 gene vs amplified REL in B cell lymphoma (DLBCL / DLBC / DLBL) -- cBioPortal

## OncoPrint
* altered/profiled:
  - PIM1: 16%
  - REL: 1%

## Cancer Types Summary

| **PIM1** |
|:-:|
| ![PIM1](images/cancer_types_summary-PIM1.png) |

| **REL** |
|:-:|
| ![REL](images/cancer_types_summary-REL.png) |

## Plots
* Copy number:

| **PIM1** |
|:-:|
![PIM1](images/copy_number-alterations-PIM1.png)

| **REL** |
|:-:|
![REL](images/copy_number-alterations-REL.png)

* Mutations:

| **PIM1** |
|:-:|
![PIM1](images/mutations_PIM1.png)

| **REL** |
|:-:|
![REL](images/mutations_REL.png)

## Conclusion
PIM1 mutations (missense) and REL amplification are implicated in B cell lymphoma.