# Exercise 2 -- macrophages (blood & immune cells)

## Step 1
### Human Protein Atlas – Single Cell Type
* blood & immune cells - The blood & immune cell transcriptome - Macrophages
  * [Cell type enhanced (445)](tables/cell_type_category_rna_Macrophages_Cell.tsv)
  * Use the [first 100 genes](tables/cell_type_category_rna_Macrophages_Cell-first100.tsv) for the subsequent data mining

### STRING

|[network (tsv)](tables/string_interactions_short-macrophages.tsv); |
|---|
|number of nodes: 97; number of edges: 211; average node degree:	4.35; avg. local clustering coefficient:	0.421; PPI enrichment p-value: < 1.0e-16 |
| ![overlap](images/string_normal_image-macrophages.png) |

* **Analysis**. Keywords interlinked of the following resources listed at STRING: Gene Ontology, KEGG, Reactome Pathways, Wiki pathways, Tissue expression, subcellular localization

```mermaid
flowchart LR;

%% Gene_Ontology
fc(Fc receptor) --- isp(inhibitory signaling pathway)
differentiation --- dc(dendritic cell)
detection --- oo(other organism)
cca(cell-cell adhesion) --- heterotypic
dr(defense response) --- cellular

uropod
granule --- al(Azurophil lumen) & f1rm(ficolin-1-rich membrane) & sl(secretory lumen) & tertiary

%% KEGG
differentiation --- osteoclast

%%Reactome_Pathways
lc(lymphoid cell) --- imr(immunoregulation) --- nlc(non-lymphoid cell)
granule --- neutrophil --- degranulation
ims(immune system) -.- innate & adaptive
homeostasis

%%Wiki_Pathways
microglia --- TYROBP --- cn(causal network)
signalling --- TCR

%% Tissue_expression
spleen
cll(CLL cell)
lymphocyte --- t(T cell)
lymphocyte -.- cll
ims -.- lt(lymphoid tissues) & leukocyte & lymphocyte
hmps(hematopoietic system) -.- ims & blood

%% Subcellular_localization
complex --- 5lpx(5-lipoxygenase) & immunoglobulin
cell --- te(trailing edge)
uropod
al
granule --- secretory & membrane
vesicle --- cytoplasmic & sv(secretory) & vm(membrane)
lysosome --- primary
vl(vacuolar lumen)
cell --- surface

```
### Reactome
| **first 100 genes**; results: [p-value < 0.05](tables/result-macrophages.csv)** |
|---|
| ![overlap](images/Reacfoam-macrophages.jpg) |

## Step 2
* [A single-cell and spatially resolved atlas of human breast cancers](https://singlecell.broadinstitute.org/single_cell/study/SCP1039/a-single-cell-and-spatially-resolved-atlas-of-human-breast-cancers)

* Genes expression per cell type **subset** (most detailed). [Table dataset](tables/singlecell-dataset-2.gct)
![SingleCell](images/SingleCell-breast-cancer-2.png)

* cell types mostly involved (list). [original study](https://www.nature.com/articles/s41588-021-00911-1):
  * Myeloid
    * **Macrophage CXCL10**. C-X-C motif chemokine ligand 10, conditions macrophages to **promote metastasis** (e.g. in lung). [35398531](https://pubmed.ncbi.nlm.nih.gov/35398531/)
    * **Macrophage EGR1**. Early growth response 1:  EGR1 (and RXRA) transcription factors in the regulation of CCL2 gene in response to **TGF-beta** pathway. [34239022](https://pubmed.ncbi.nlm.nih.gov/34239022/)
    * **Macrophage SIGLEC1**. Siglecs that binds cell surface sialoglycans are a family of immunomodulatory receptors. Siglec-1 expressed on macrophages on tumor development remains largely **unexplored**. [34425200](https://pubmed.ncbi.nlm.nih.gov/34425200/)
    * **Monocyte S100A9**. The protein S100A9 is an essential regulator of the  **tumor microenvironment**, associated with poor prognosis. [33401528](https://pubmed.ncbi.nlm.nih.gov/33401528/)
    * **Monocyte IL1B**. IL1β-mediated **protumor inflammation** in (lung) cancer. [33023946](https://pubmed.ncbi.nlm.nih.gov/33023946/)
    * **Monocyte FCGR3A**. hFcgammaRIIIa activation plays a significant role on antibody-dependent **cellular cytotoxicity** by human peripheral monocytes. [30984171](https://pubmed.ncbi.nlm.nih.gov/30984171/)
    * **Cycling**. Involved in **mestastasis**? [33983582](https://pubmed.ncbi.nlm.nih.gov/33983582/)
    * **Dendritic cells**: associations between DC subpopulations and histological and clinical characteristics, as well as molecular subtypes in breast carcinoma. [33919875](https://pubmed.ncbi.nlm.nih.gov/33919875/)
      * **cDC2 CD1C**. Conventional dendritic cells (cDC) type-2. cDC**3** inflammatory DCs endowed with a strong potential to **regulate tumor immunity**. [32610077](https://pubmed.ncbi.nlm.nih.gov/32610077/)
      * **DC LAMP3**. LAMP3 is highly expressed only in certain cell types and during the differentiation stages. Its expression is linked the **maturation** of dendritic cells, **inflammation**, poor prognosis of certain tumors. [27697285](https://pubmed.ncbi.nlm.nih.gov/27697285/)
      * **cDC1 CLEC9A**.  CLEC9A antibodies deliver antigen specifically to cDC1 for the **induction of humoral, CD4+ and CD8+ T cell responses** and are therefore promising candidates to develop as vaccines for infectious diseases and cancer. [33625943](https://pubmed.ncbi.nlm.nih.gov/33625943/)
      * **pDC IRF7**. plasmacytoid DC (pDC) produce the highest amounts of type I IFN. IRF-7, activated upon TLR signaling is required for IFN induction not only in pDC, but also in conventional DC (cDC) and non-DC cell types. [16474425](https://pubmed.ncbi.nlm.nih.gov/16474425/)
  * Lymphoid
    * **B cells memory**
    * **B cells naïve**
    * **T cells MKI67**. MKI67 may be involved in BC's carcinogenesis, **development**, or **metastasis**. [35601002](https://pubmed.ncbi.nlm.nih.gov/35601002/)
    * **T cells CD8+ LAG3**. Lymphocyte-activation gene 3. Inhibition of LAG3 with TSR-033 results in a significant increase in calcium fluctuations of CD8+ T cells in contact with dendritic cells. The combination of TSR-042 and TSR-033 appears to synergistically increase **tumor cell killing** and the single-cell level. [33188167](https://pubmed.ncbi.nlm.nih.gov/33188167/)
    * **T cells CD8+ ZFP36**. DNA binding protein ZFP36 (and ZFP36L1) limit the rate of **differentiation** of activated naive CD8+ T cells and the **potency** of the resulting **cytotoxic** lymphocytes. [35477960](https://pubmed.ncbi.nlm.nih.gov/35477960/)
    * **T cells CD8+ GZMK**. granzyme K. Undifferentiated pre-effector/memory T cells (TCF7+, GZMK+) or inhibitory macrophages (CX3CR1+, C3+) are **inversely** correlated with T cell **expansion**. [33958794](https://pubmed.ncbi.nlm.nih.gov/33958794/)
    * **T cells CD8+ IFNG**. Elevated levels of CD8+Ki67+ T cells producing IFN-γ were associated with **improved progression-free survival** and overall survival (OS). High frequencies of these T cells were correlated with significantly **reduced risk of death** over time. [31391334](https://pubmed.ncbi.nlm.nih.gov/31391334/)
    * **T cells NKT FCGR3A**. Higher stromal GZMB+ and FCGR3A+ NK cell densities associate with **longer cancer-specific survival**. [34937729](https://pubmed.ncbi.nlm.nih.gov/34937729/)
    * **T cells NK AREG**. Amphiregulin. AREG, as one of the main ligands of the EFGR pathway, mainly participates in the **regulation** of **proliferation, apoptosis, and metastasis of various cells**. [34671607](https://pubmed.ncbi.nlm.nih.gov/34671607/)
    * **T cells CD4+ T-regs FOXP3**. Myeloid-derived suppressor cells (MDSCs) and CD4+Foxp3+ Tregs are enriched within tumors and at peripheral sites such as spleen and peripheral blood where they **potently suppress antitumor response**. [24105680](https://pubmed.ncbi.nlm.nih.gov/24105680/)
    * **T cells CD4+ Tfh FOXP3**. T follicular regulatory cells suppress Tfh-mediated B cell help and synergistically **increase IL-10-producing B cells in breast carcinoma**. [31440888](https://pubmed.ncbi.nlm.nih.gov/31440888/)
    * **T cells CD4+ CCR7**. C-C Chemokine receptor 7 (CCR7), binds chemokines CCL19 and CCL21, which are important for **tissue homeostasis**, **immune surveillance** and **tumorigenesis**. [32340161](https://pubmed.ncbi.nlm.nih.gov/32340161/)
    * **T cells CD4+ IL-7R**. Interleukin-7 is a non-redundant **growth**, **differentiation** and **survival factor** for human T lymphocytes. [25130296](https://pubmed.ncbi.nlm.nih.gov/25130296/)
  * Endothelial [info collected in the previous assignment](1_fibroblasts.md)
    * **Endothelial Lymphatic LYVE1**.
    * **Endothelial ACKR1**.
    * **Endothelial RGS5**.
    * **Endothelial CXCL12**

* The chart below *does not* aim to represent the lineage development of the cells, but rather the individual cells from the study.
```mermaid
flowchart TB;
myeloid --- macrophages & monocytes & dc(dendritic cells)
macrophages -.- CXCL10 & EGR1 & SIGLEC1
monocytes -.- S100A9 & IL1B & FCGR3A
dc -.- cDC2(cDC2 CD1C) & dcl(DC LAMP3) & cDC1(cDC1 CLEC9A) & pDC(pDC IRF7)
```
```mermaid
flowchart TB;
lymphoid --- tc(T cells) & bc(B cells)
bc -.- memory & naïve
tc -.- CD4+ & CD8+ & NK
CD4+ -.- T-regs & Tfh & CCR7 & IL-7R
CD8+ -.- ZFP36 & GZMK & IFNG & LAG3
NK -.- f(FCGR3A) & AREG
```

* [Short list of genes (16)](tables/myeloid_lymhpoid.tsv) overexpressed in the cell types (**both** myeloid and lymhpoid) mostly involved.
* Most strongly expressed genes: SRGN, ARHGDIB, CD52, CORO1A, HCST

## Analysis
|STRING [network (tsv)](tables/string_interactions_short-16.tsv) of the 16 genes|
|---|
|number of nodes: 16; number of edges: 26; average node degree:	3.25; avg. local clustering coefficient:	0.487; PPI enrichment p-value: < 1.0e-16 |
| ![overlap](images/string_normal_image-16.png) |

* Most strongly expressed genes, coded proteins details:

| protein topology | info |
|---|---|
|![SRGN](images/SRGN.svg)| **SRGN**: Proteoglycan peptide core protein; Serglycin; Plays a role in formation of mast cell **secretory granules** and mediates storage of various compounds in **secretory vesicles**. Required for storage of some proteases in both connective tissue and mucosal mast cells and for storage of **granzyme B** in **T-lymphocytes**. Plays a role in localizing neutrophil elastase in azurophil granules of neutrophils. Mediates processing of MMP2. Plays a role in **cytotoxic cell** granule-mediated **apoptosis** by forming a complex with granzyme B which is delivered to cells by perforin to induce apoptosis. Regulates the sec [...] ; |
|![ARHGDIB](images/ARHGDIB.svg)| **ARHGDIB**: Rho GDP-dissociation inhibitor 2; Regulates the **GDP/GTP exchange** reaction of the Rho proteins by inhibiting the dissociation of GDP from them, and the subsequent binding of GTP to them. Regulates reorganization of the **actin cytoskeleton** mediated by Rho family members ; |
|![CD52](images/CD52.svg)| **CD52**: CAMPATH-1 antigen; May play a role in carrying and orienting carbohydrate, as well as having a more specific role; CD molecules. It has been conjectured that its function is **anti-adhesion**, allowing cells to freely move around. ; |
|![CORO1A](images/CORO1A.svg)| **CORO1A**: Coronin-1A; May be a crucial component of the **cytoskeleton** of **highly motile cells**, functioning both in the invagination of large pieces of plasma membrane, as well as in forming protrusions of the plasma membrane involved in cell locomotion. In mycobacteria- infected cells, its retention on the phagosomal membrane prevents fusion between **phagosomes** and **lysosomes**; Belongs to the WD repeat coronin family ; |
|![HCST](images/HCST.svg)| **HCST**: Hematopoietic cell signal transducer; Transmembrane **adapter protein** which associates with KLRK1 to form an **activation receptor** KLRK1-HCST in lymphoid and myeloid cells; this receptor plays a major role in **triggering cytotoxicity** against target cells expressing cell surface ligands such as MHC class I chain-related MICA and MICB, and UL16-binding proteins (ULBPs); these **ligands are up-regulated** by stress conditions and pathological state such as **viral** infection and **tumor** transformation. Functions as docking site for PI3-kinase PIK3R1 and GRB2. Interaction of ULBPs with KLRK1-HCST trigge [...] ; |

| **Most strongly expressed genes**; [16](tables/result-16.csv) |
|---|
| ![overlap](images/Reacfoam-16.jpg) |
* Top 5 processes, sorted by Entities ratio (highest -> lowest)
1. Immune System
2. Adaptive Immune System
3. Hemostasis
4. Platelet activation, signaling and aggregation
5. Cargo concentration in the ER

### Conclusions
Considering the genes list, the cells mostly expressing the genes in the breast cancer study are immune system cells. The majority of the genes are expressed by the adaptive immunity cells, such as T and B. A subset of 16 genes are also strongly expressed by the innate (myeloid) immune system cells, such as macrophares, monocytes and dendritic cells. The cell types suggest both a pro- (e.g. T cells MKI67, *T cells CD4+ T-regs FOXP3, Tfh FOXP3, IL-7R) and anti- (e.g T cells CD8+ LAG3, IFNG, NKT FCGR3A) roles, whereas abnormalities in the machanisms of the latter may promote neoplasia. Roughly, it appears that CD+ T cells would have a positive role for tumor progression while CD8+ T cells would have anti-tumor effect. The subset of 16 genes common for both innate and adaptive cells was used for the subsequent analysis at STRING and REACTOME. The REACTOME analyses strongly pointed towards the immune system, as well as homeostasis maintenance and signalling. They 16 genes form a tightly interconnected cluster in STRING, where the top 5 genes (by expression levels) are involved in cytotoxicity (grnzB), cytoskeleton reorganization and its related Rho-familt GEFs, as well as cell motility and adhesion.
