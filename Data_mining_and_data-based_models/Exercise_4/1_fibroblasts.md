# Exercise 1 -- fibroblasts (mesenchymal cells)

## Step 1
### Human Protein Atlas – Single Cell Type
* Mesenchymal cells - Fibroblasts - cell type
  * [enhanced (408)](tables/cell_type_category_rna_Fibroblasts_Cell.tsv)
  * Use the [first 100 genes](tables/cell_type_category_rna_Fibroblasts_Cell-first100.tsv) for the subsequent data mining

### STRING

|[network (tsv)](tables/string_interactions_short-fibroblasts.tsv); |
|---|
|number of nodes: 96; number of edges: 90; average node degree:	1.88; avg. local clustering coefficient:	0.379; PPI enrichment p-value: < 1.0e-16 |
|notable cluster of **ADAMTS** proteins|
| ![overlap](images/string_normal_image-fibroblasts.png) |

* **Analysis**. Keywords interlinked of the following resources listed at STRING: Gene Ontology, KEGG, Reactome Pathways, Wiki pathways, Diseases, Tissue expression, subcellular localization

```mermaid
flowchart LR;

%% Gene_Ontology
mf(Molecular function) --- pa(peptidase activity) & hb(heparin ninding)
pa --- metalloendo & metallo & endo
cc(Cellular component) --- extracellular & bmp(blood microparticle)
extracellular --- matrix & region & space
matrix --- cg(collagen containing) 

%% KEGG
abc(ABC transporters)
complement
coagulation
diseases --- sai(Staphylococcus aureus infection) & sle(Systemic lupus erythematosus) & Pertussis

%%Reactome_Pathways
diseases --- pps(Defective B3GALTL causes Peters-plus syndrome )
abc(ABC transporters) --- lh(lipid homeostasis)
O-glycosylation --- tsr(TSR domain-containing proteins)
complement --- it(initial triggering) & regulation

%%Wiki_Pathways
ace(ACE inhibitor pathway)
complement --- activation & cascades & system
coagulation --- cascades

%%Diseases
diseases --- ed(Ehlers-Danlos syndrome) & ct(Connective tissue disease) & msk(Musculoskeletal system disease)

%% Tissue_expression
pc(Plasma cell) -.- Tissue_expression
bmc(Bone marrow cell) -.- Tissue_expression

%% Subcellular_localization 
tmb(Thrombospondin complex)
complement --- c1q(c1q complex)
%%extracellular --- region & space
blood --- microparticle
```
### Reactome
| **first 100**; [top 25](tables/result-fibroblasts.csv)** |
|---|
| ![overlap](images/Reacfoam-fibroblasts.jpg) |

## Step 2
* [A single-cell and spatially resolved atlas of human breast cancers](https://singlecell.broadinstitute.org/single_cell/study/SCP1039/a-single-cell-and-spatially-resolved-atlas-of-human-breast-cancers)

* Genes expression per cell type:
  * [table dataset](tables/singlecell-dataset.gct)

![SingleCell](images/SingleCell-breast-cancer.png)
* cell types mostly involved (list). [original study](https://www.nature.com/articles/s41588-021-00911-1):
  * Endothelial
    * **Endothelial ACKR1**. ACKR1 is important for ligand transcytosis across endothelial layers, to enable presentation in the vascular lumen, as well as in buffering inflammatory chemokine levels in the circulation. [30800123](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6375834/)
    * **Endothelial RGS5**. RGS5 is a regulator of G-protein signaling, involved in perycite-driven tumor maturation [25069475](https://pubmed.ncbi.nlm.nih.gov/25069475/)
    * **Endothelial CXCL12**. CXC12 activation of endothelial cells and angiogenesis. [29390073](https://pubmed.ncbi.nlm.nih.gov/29390073/)
    * **Endothelial Lymphatic LYVE1**. LYVE-1 is a widely used marker for lymphatic endothelial cells (LEC), which also mediates immune and cancer cell migration. [34775672](https://pubmed.ncbi.nlm.nih.gov/34775672/)
  * CAFs (cancer-associated fibroblasts) [33633222](https://pubmed.ncbi.nlm.nih.gov/33633222/)
    * **CAFs MSC iCAF-like s1 & s2**. MSC: mesenchymal stem cells; iCAF: inflammatory CAFs; located further away from the cancer cells and characterized by the secretion of inflammatory mediators. Highly associated with immune evasion [32790115](https://pubmed.ncbi.nlm.nih.gov/32790115/)
    * **CAFs Transitioning s3**.
    * **CAFs myCAF like s4 & s5**. myCAF: myoblastic CAFs; located immediately adjacent to the cancer cells and demonstrate elevated αSMA expression
  * PVL (perivascular-like) [32790115](https://pubmed.ncbi.nlm.nih.gov/32790115/)
    * **PVL Immature s1 & s2**. Associated with good survival outcome in breast cancer.
    * **PVL Differentiated s3**. Highly associated with immune evasion.
    * **Cycling PVL**.

```mermaid
flowchart TB;
Endothelial --- ACKR1 & RGS5 & CXCL12 & Lymphatic
Lymphatic --- LYVE1
CAFs --- MSC & ts3(Transitioning s3) & myCAF(myCAF like)
myCAF --- ms4(s4) & ms5(s5)
MSC --- iCAF(iCAF-like) --- is1(s1) & is2(s2)
PVL --- ps3(Differentiated s3) & Immature & cpvl(Cycling PVL)
Immature ---  ims1(s1) & ims2(s2)
```

* [Short list of genes (18)](tables/singlecell-18.tsv) overexpressed in the cell types mostly involved.
* Most strongly expressed genes: C1R, C1S, AEBP1, ADAMTS1, APOD

## Analysis
|STRING [network (tsv)](tables/string_interactions_short-18.tsv) of the 18 genes|
|---|
|number of nodes: 18; number of edges: 9; average node degree:	1; avg. local clustering coefficient:	0.556; PPI enrichment p-value: < 1.41e-09 |
| ![overlap](images/string_normal_image-18.png) |

* Most strongly expressed genes, coded proteins details:

| protein topology | info |
|---|---|
|![C1R](images/C1R.svg)| **C1R**: Complement C1r subcomponent; C1r B chain is a serine protease that combines with C1q and C1s to form C1, the first component of the classical pathway of the **complement system**; Belongs to the peptidase S1 family; |
|![C1S](images/C1S.svg)| **C1S**: Complement C1s subcomponent; C1s B chain is a serine protease that combines with C1q and C1r to form C1, the first component of the classical pathway of the **complement system**. C1r activates C1s so that it can, in turn, activate C2 and C4; |
|![AEBP1](images/AEBP1.svg)| **AEBP1**: Adipocyte enhancer-binding protein 1; May positively regulate MAP-kinase activity in adipocytes, leading to **enhanced adipocyte proliferation** and reduced adipocyte differentiation. May also positively regulate NF-kappa-B activity in macrophages by promoting the phosphorylation and subsequent degradation of I-kappa-B-alpha (NFKBIA), leading to enhanced macrophage inflammatory responsiveness. Can act as a transcriptional repressor; M14 carboxypeptidases; |
|![ADAMTS1](images/ADAMTS1.svg)| **ADAMTS1**: A disintegrin and metalloproteinase with thrombospondin motifs 1; Cleaves aggrecan, a cartilage proteoglycan, at the '1938-Glu-/-Leu-1939' site (within the chondroitin sulfate attachment domain), and may be involved in its turnover (By similarity). Has **angiogenic inhibitor activity**. Active metalloprotease, which may be associated with various **inflammatory processes** as well as development of cancer cachexia. May play a critical role in follicular rupture; ADAM metallopeptidases with thrombospondin type 1 motif; |
|![APOD](images/APOD.svg)| **APOD**: Apolipoprotein D; APOD occurs in the macromolecular complex with lecithin- cholesterol acyltransferase. It is probably involved in the **transport and binding of bilin**. Appears to be able to transport a variety of ligands in a number of different contexts; Apolipoproteins; |

| **Most strongly expressed genes**; [18](tables/result-18.csv) |
|---|
| ![overlap](images/Reacfoam-18.jpg) |
* Top 5 processes, sorted by Entities ratio (highest -> lowest)
1. Diseases of metabolism
2. Extracellular matrix (ECM) organization
3. Diseases of glycosylation
4. Complement cascade
5. Degradation of the extracellular matrix

### Conclusions
Considering the genes list, the cell types most expressing the genes are endothelial, CAFs and PVLs. Each cell type is subdivided into several subtypes, dependent on marker expression and/or developmental stage. The subtypes are implicated differently in cancer, via signalling mechanisms (ACKR1, RGS5, CXCL12 for endothelial cells), proximity to tumor cells (MSC iCAF vs myCAFs) and survival outcome prognosis (PVL Immature) or immune evasion (PVL differentiated). The genes are implicated in metabolism disruption (diseases), organization and degradation ot the ECM, glycosylation diseases and hence disorders of extracellular proteins structure and function, as well as complement system. The top 5 genes point towards the complement system (C1R/S), enhancer binding proteins (AEBP1), as well as angiogenesis and proliferation -- two processes that are greatly involved in oncogenesis.
