# APC

## PubMed:
* **colorectal**
  - gastric epithelia
  - colon
* other: pancreatic, breast, prostate

## [GeneCards](https://www.genecards.org/cgi-bin/carddisp.pl?gene=APC):
* APC Regulator Of WNT Signaling Pathway
* External Ids for APC Gene
  - HGNC: 583
  - NCBI Entrez Gene: 324
  - Ensembl: ENSG00000134982
  - OMIM: 611731
  - UniProtKB/Swiss-Prot: P25054
* Summary for APC Gene
  > Mutations in the APC gene have been found to occur in most
  > colorectal cancers. Disease-associated mutations tend to be
  > **clustered in a small region** designated the mutation cluster
  > region (MCR) and result in a truncated protein product
  - tumor suppressor
  - degradation of CTNNB1
  - antagonist (negative regulator) of the Wnt signaling pathway
  - cell migration and adhesion
  - transcriptional activation
  - apoptosis
  - Activates the GEF activity of SPATA13 and ARHGEF4
  - MMP9 up-regulation via the JNK signaling pathway

## [PubTator](https://www.ncbi.nlm.nih.gov/research/pubtator/?view=docsum&query=apc%20colorectal%20cancer)
* MMR & TGFBR2; KRAS (drug target); IL-25/ILC2/MDSC; HER2 (G776S); EGFR
* beta-Catenin; AP4/MDC1/MIR22HG/miR-22-3p; TDO2; CDX2; SDC2/SFRP2; BCL-3; GXYLT1;
   
## [OncoKB](https://www.oncokb.org/gene/APC)
* Tumor suppressor
  > APC, a tumor suppressor involved in WNT signaling, is recurrently
  > altered in colorectal cancer. There are **no FDA-approved or NCCN-
  > compendium listed treatments**
* 70% altered in colorectal cancer
* **R640G** mutation (Loss-of-function) is known to be **oncogenic**
* Deletion (copy number loss resulting in partial or whole deletion): *likely oncogenic*; 
* Truncating Mutations: *likely oncogenic*
* N1026S: *likely oncogenic*

## [KEGG](https://www.genome.jp/dbget-bin/www_bget?pathway:map05210)
* Proliferation induced by:
  - c-Myc
  - CyclinD1
  - Survivin (anti-apoptotic)

## [GeneOntology](http://amigo.geneontology.org/amigo/search/annotation?q=apc)
* Organism: *H. sapiens*
```mermaid
pie showData
    title GO class (direct) >10
    "protein binding" : 140
    "cytosol" : 72
    "Wnt neg regulator" : 15
    "beta-catenin bind" :  14
    "plasma membrane" : 14
```
* Ontology (aspect P): protein folding

## [The human protein atlas](https://www.proteinatlas.org/ENSG00000134982-APC/tissue)
* Tissue / Gastrointestinal tract
  - stomach: glandular cells (medium)
  - **colon**: endothelial cells (low); glandular cells (medium)
  - duodenum: glandular cells (high)
  - **rectum**: glandular cells (high)
  - small intestine: glandular cells (high)
* Pathology
  - 9/12 patients show high/medium expression in colorectal cancer
  - [protein expression](https://www.proteinatlas.org/ENSG00000134982-APC/pathology/colorectal+cancer#img)

## [Mouse phenotype](https://www.mousephenotype.org/data/genes/MGI:88039)
* Not currently registered for phenotyping at IMPC
* Phenotyping is currently not planned for a knockout strain of this gene

## Summary
```mermaid
   flowchart BT;
   
   apc[[APC]] --- Protein_Atlas --- pat(9/12 patients show high/medium expression ) ---|link to| id2((Colorectal cancer));
   id2 ---- id4(no FDA-approved or NCCN-compendium listed treatments)
   apc -..- pb(protein binding)
   apc -...- pf(protein folding)
   apc ----|oncogenic| R640G ---- id2
   apc --x |antagonist| wnt(Wnt signalling)
   apc --x |degradation| CTNNB1
   apc --- JNK --- MMP9 --- id2

   subgraph Protein_Atlas
   colon -- medium --> glc(glandular cells)
   colon -- low --> endc(endothelial cells)
   rectum -- high --> glc
   end
   
   id2 -. prolif .-  c-Myc
   id2 -. prolif .- Cyclind1
   id2 -. anti-apo .- Survivin
  
   MMR -.- apc
   TGHBR -..- apc
   KRAS -.- apc
   IL-25/ILC2/MDSC -..- apc
   HER2 -.- apc
   EGFR -..- apc
   beta-Catenin -.- apc
   AP4/MDC1/MIR22HG/miR-22-3p -..- apc
   TDO2 -.- apc
   CDX2 -..- apc
   SDC2/SFRP2 -.- apc
   BCL-3 -..- apc
   GXYLT1 -.- apc

```
