# HER2

## [PubMed](https://pubmed.ncbi.nlm.nih.gov/?term=HER2+cancer&filter=pubt.clinicaltrial&filter=pubt.systematicreview):
* **breast**
* gastric
* crvical

## [GeneCards](https://www.genecards.org/cgi-bin/carddisp.pl?gene=ERBB2):
* Erb-B2 Receptor Tyrosine Kinase 2
* External Ids for ERBB2 Gene
  - HGNC: 3430
  - NCBI Entrez Gene: 2064
  - Ensembl: ENSG00000141736
  - OMIM: 164870
  - UniProtKB/Swiss-Prot: P04626
* Summary for HER2 gene:
  > Amplification and/or overexpression of this gene has been reported in
  > numerous cancers, including **breast** and ovarian tumors
  - EGF family, no ligand-binding domain
  - binds to other ligand-bound EGF receptors
  - allelic variants at  Ile654/Ile655
  - HER2-targeted therapy
  - TFAP2 family pathway
  - Prolactin signaling

## [PubTator](https://www.ncbi.nlm.nih.gov/research/pubtator/?view=docsum&query=HER2%20breast%20cancer)
* PIK3CA; ZNF582-AS1; EpCAM/EGFR; GBPs; GRHL2; HR;
   
## [OncoKB](https://www.oncokb.org/gene/ERBB2)
* a receptor tyrosine kinase
  > altered by mutation, amplification and/or overexpression in various cancer
  > types, most frequently in **breast**, esophagogastric and endometrial cancers.
* 74 oncogenic mutations
* response to drug: Neratinib

## [KEGG](https://www.genome.jp/dbget-bin/www_bget?pathway:map05224)
* Survival, Proliferation, Translation:
  - ERK1/2
  - S6K

## [GeneOntology](http://amigo.geneontology.org/amigo/search/annotation?q=ERBB2)
* Organism: *H. sapiens*
```mermaid
pie showData
    title GO class (direct) >10
    "protein binding" : 186
    "plasma membrane" : 104
```
* Ontology (aspect P): signaling

## [The human protein atlas](https://www.proteinatlas.org/ENSG00000141736-ERBB2/tissue)
* Tissue / Breast
  - adipocytes: n/d
  - glandular cells: medium
  - Myoepithelial cells: medium
* Pathology
  - 6/11 patients show high/medium expression in breast cancer
  - [protein expression](https://www.proteinatlas.org/ENSG00000141736-ERBB2/pathology/breast+cancer#img)

## [Mouse phenotype](https://www.mousephenotype.org/data/genes/MGI:95410)
* increased mean corpuscular hemoglobin (HET)
* preweaning lethality, complete penetrance (HOM)

## Summary
```mermaid
   flowchart BT;
   
   her2[[HER2]] --- Protein_Atlas --- pat(6/11 patients show high/medium expression ) ---|link to| id2((Breast cancer));
   id2 ---- id4(response to Neratinib drug)
   her2 -..- eb(bind ligand-bound EGFRs)
   her2 -..- pb(protein binding)
   her2 -----|oncogenic| 72(72  mutations) ---- id2
   her2 --- |pathway| TFAP2 & Prolactin

   subgraph Protein_Atlas
   breast -- medium --> glc(glandular cells)
   breast -- medium --> endc(myoepithelial cells)
   end
   
   id2 -. prolif .-  ERK-1/2
   id2 -. prolif .- S6K

    PIK3CA -.- her2
    ZNF582-AS1 -.- her2
    EpCAM/EGFR -.- her2
    GBPs -.- her2
    GRHL2 -.- her2
    HR -.- her2

```