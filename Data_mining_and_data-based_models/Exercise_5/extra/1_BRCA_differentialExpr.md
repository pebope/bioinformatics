# BRCA
* Differentially expressed genes between stages:

|I_IB|I_II|I_IIA|I_IIB|I_III|
|:-:|:-:|:-:|:-:|:-:|
|![I_IB](images/BRCA-stage-I_IB-mRNA.png)|![](images/BRCA-stage-I_II-mRNA.png)|![](images/BRCA-stage-I_IIA-mRNA.png)|![](images/BRCA-stage-I_IIB-mRNA.png)|![](images/BRCA-stage-I_III-mRNA.png)|

|I_IIIA|I_IIIB|I_IV|IA_IB|IA_II|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-I_IIIA-mRNA.png)|![](images/BRCA-stage-I_IIIB-mRNA.png)|![](images/BRCA-stage-I_IV-mRNA.png)|![](images/BRCA-stage-IA_IB-mRNA.png)|![](images/BRCA-stage-IA_II-mRNA.png)|

|IA_III|IA_IIIB|IA_IV|IB_IIA|IB_IIB|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-IA_III-mRNA.png)|![](images/BRCA-stage-IA_IIIB-mRNA.png)|![](images/BRCA-stage-IA_IV-mRNA.png)|![](images/BRCA-stage-IB_IIA-mRNA.png)|![](images/BRCA-stage-IB_IIB-mRNA.png)|

|IB_IIIA|IB_IIIC|II_IIA|II_IIB|II_IIIA|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-IB_IIIA-mRNA.png)|![](images/BRCA-stage-IB_IIIC-mRNA.png)|![](images/BRCA-stage-II_IIA-mRNA.png)|![](images/BRCA-stage-II_IIB-mRNA.png)|![](images/BRCA-stage-II_IIIA-mRNA.png)|

|II_IIIB|II_IIIC|II_IV|IIA_III|IIA_IIIA|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-II_IIIB-mRNA.png)|![](images/BRCA-stage-II_IIIC-mRNA.png)|![](images/BRCA-stage-II_IV-mRNA.png)|![](images/BRCA-stage-IIA_III-mRNA.png)|![](images/BRCA-stage-IIA_IIIA-mRNA.png)|

|IIA_IIIB|IIA_IIIC|IIA_IV|IIB_III|IIB_IIIB|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-IIA_IIIB-mRNA.png)|![](images/BRCA-stage-IIA_IIIC-mRNA.png)|![](images/BRCA-stage-IIA_IV-mRNA.png)|![](images/BRCA-stage-IIB_III-mRNA.png)|![](images/BRCA-stage-IIB_IIIB-mRNA.png)|


|IIB_IIIC|IIB_IV|III_IIIA|III_IIIB|III_IIIC|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-IIB_IIIC-mRNA.png)|![](images/BRCA-stage-IIB_IV-mRNA.png)|![](images/BRCA-stage-III_IIIA-mRNA.png)|![](images/BRCA-stage-III_IIIB-mRNA.png)|![](images/BRCA-stage-III_IIIC-mRNA.png)|

|III_IV|IIIA_IIIB|IIIA_IV|IIIB_IIIC|IIIC_IV|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-stage-III_IV-mRNA.png)|![](images/BRCA-stage-IIIA_IIIB-mRNA.png)|![](images/BRCA-stage-IIIA_IV-mRNA.png)|![](images/BRCA-stage-IIIB_IIIC-mRNA.png)|![](images/BRCA-stage-IIIC_IV-mRNA.png)|