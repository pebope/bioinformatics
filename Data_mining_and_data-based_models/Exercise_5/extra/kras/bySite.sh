#!/bin/bash

CWD=$(pwd)

cd $CWD/bySite
for i in * ; do
  hstype=$(cat $i | datamash --field-separator=, -sg 1 count 1)
  gender=$(cat $i | datamash --field-separator=, -sg 2 count 2)
  
  echo "${i%.*}" >> $CWD/bySite.csv
  echo "$hstype" >> $CWD/bySite.csv
  echo "$gender" >> $CWD/bySite.csv
  echo "" 
done
