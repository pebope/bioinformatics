#!/bin/bash

CWD=$(pwd)

mkdir -p $CWD/bySite 

# put column headers
echo "histological_type,gender,aminoAcid,sample" > byType.csv

sed 1d somatic_mutation.csv | while IFS=, read -r \
sample chr start end gene reference alt altGene effect aminoAcid rnaVaf dnaVaf ; do
  hstype=$(grep "$sample" histological_type.csv | awk -F ',' '{print $2}')
  gender=$(grep "$sample" gender.tsv | awk '{print $2}')
  
  # create a simple spreadsheet of hist. type and mutations
  echo "${hstype},${gender},${aminoAcid},${sample}" >> byType.csv
  
  # sort by amino acid
  echo "${hstype},${gender}" >> bySite/${aminoAcid}.csv
done
