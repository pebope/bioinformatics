# Mutated BRCA1 gene vs amplified ERBB2 in breast cancer (BRCA) -- cBioPortal

## Forward approach: BRCA1/ERBB2
* breast cancer (BRCA), [TCGA, Cell 2015](https://www.ncbi.nlm.nih.gov/pubmed/26451490)
* Group A: Mutated genes - BRCA1 (#Mut: 18, Freq: 2.2%)
* Group B: CNA genes - ERBB2 (#Samples: 105, Freq: 12.9%)

### Compare groups - Clinical (p-value < 0.05)
|IHC-HER2|IHC-score|HER2-fish|staging_system|HER2_cent17-ratio|
|:-:|:-:|:-:|:-:|:-:|
|![IHC-HER2](images/IHC-HER2.png)|![IHC-score](images/HER2-ihc-score.png)|![HER2-fish](images/HER2-fish.png)|![staging_system](images/BRCA1_ERBB2-staging_system.png)|![HER2_cent17](images/HER2_cent17-ratio.png)|

* **HER2**. Whether or not the cancer cells have HER2 receptors and/or hormone receptors on their surface. HER2 status is of great clinical value in breast tumors for the identification of those patients who are eligible for (trastuzumab) therapy. [18685491](https://pubmed.ncbi.nlm.nih.gov/18685491/)
* [IHC score](https://www.cancer.org/cancer/breast-cancer/understanding-a-breast-cancer-diagnosis/breast-cancer-her2-status.html)
  * 0-1+: the cancer is considered HER2-negative. These cancers do not respond to treatment with drugs that target HER2
  * 3+: he cancer is HER2-positive. These cancers are usually treated with drugs that target HER2.
  * 2+: the HER2 status of the tumor is not clear and is called "equivocal." This means that the HER2 status needs to be tested with **FISH to clarify the result:**
* **HER2/Cent17 ratio**: Various mechanisms have been proposed for trastuzumab resistance, such as high HER2 to Chromosome 17 FISH (HER2/CEP17) ratios and the possibility that single agent trastuzumab may not suffice to efficiently block HER2 downstream signaling thresholds. [PMC4963084](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4963084/)

> **Remarks:**
> * Group **"ERBB2:AMP - amplified"** seem to be mostly positive for HER2. Naturally, since [ERBB2 is HER2](https://www.ncbi.nlm.nih.gov/gene/2064)
> * Group **BRCA - mutated** seem to be mostly negative for HER2.

### Compare groups - mRNA
* Differentially expressed genes already covered in [Exercise 3](../Exercise_3/1_BRCA.md)

## Reverse approach: Summary - Charts - Clinical
### Neoplasm Disease Stage American Joint Committee on Cancer
* Code ID: AJCC_PATHOLOGIC_TUMOR_STAGE
* The extent of a cancer, especially whether the disease has spread from the original site to other parts of the body based on AJCC staging criteria.
* cBioPortal:
```mermaid
%% GitLab has a boring default theme...
%% themes: base, dark, default, forest, neutral 
%%{init: {'theme': 'base' } }%%
pie showData
title Neoplasm disease stage
"IIA" :	269
"IIB" :	189
"IIIA" :	112
"I" :	75
"IA" :	60
"IIIC" :	48
"IIIB" :	22
"IV" :	13
"X" :	10
"IB" :	5
"II" :	3
"III" :	2
"NA" :	9
```
* [Stages chart:](https://www.medicinenet.com/what_are_the_4_stages_of_breast_cancer/article.htm)

```mermaid
flowchart TB;
0(stage 0) --> I(stage I) ---> II(stage II) --> III(stage III) ----> IV(stage IV)
I --- IA
I --- IB
II --- IIA
II --- IIB
III --- IIIA
III --- IIIB
III --- IIIC

IB ------ ln(LN: lymph nodes)
IIA & IIB -..-|may| ln
IIIA ---|<9LN underarm/collarbone|ln
IIIC ---|>10LN above/below collarbone|ln
IIIB --- cws(chest wall or skin)
IV ---|spread|spread(far away)
```

* **Highest** number of differentially expressed genes was found between (> 600 genes):

|I_IIA (1206)|IIA_III (777)|IIB_III (765)|III_IIIA (723)|IA_III (666)|
|:-:|:-:|:-:|:-:|:-:|
|![](extra/images/BRCA-stage-I_IIA-mRNA.png)|![](extra/images/BRCA-stage-IIA_III-mRNA.png)|![](extra/images/BRCA-stage-IIB_III-mRNA.png)|![](extra/images/BRCA-stage-III_IIIA-mRNA.png)|![](extra/images/BRCA-stage-IA_III-mRNA.png)|

* The **complete** list of pairwise comparisons [can be found here](extra/1_BRCA_differentialExpr.md). It's overwhelmingly long to analyse all for this exercise.
* Overall comparison between stages, focusing on ER, HER and PR (exclude X, NA, as well as, stages with low number of samples: IB (5), II(3), III(2)), as well as between stages I and IIA:

|ER_Status_by_IHC|HER_fish_status|PR_status_ihc_percent_positive|ER_Status_by_IHC: I_IIA|HER_fish_status: I_IIA|
|:-:|:-:|:-:|:-:|:-:|
|![](images/BRCA-ER_Status_by_IHC.png)|![](images/BRCA-HER_fish_status.png)|![](images/BRCA_PR_status_ihc_percent_positive.png)|![](images/BRCA-ER_Status_by_IHC-I_IIA.png)|![](images/BRCA-HER_fish_status-I-IIA.png)|

* [Quick notes](https://www.webmd.com/breast-cancer/breast-cancer-types-er-positive-her2-positive)
  * **ER**: estrogen receptor. About 80% of all breast cancers are “ER-positive.” That means the cancer cells grow in response to the hormone estrogen. 
  * **HER**: In about 20% of breast cancers, the cells make too much of a protein known as HER2. These cancers tend to be aggressive and fast-growing. Also, covered above.
  * **PR**: progesterone receptor. About 65% of these are also “PR-positive.” They grow in response to another hormone, progesterone.

> **Remarks**
>
> From the data, it looks like ER is important in the very onse of BRCA at stage I, while HER2begins to play a role at a later stage, e.g. IIA. Data from PR is *a bit* difficult to draw conclusions from.

* Consult Reactome. Compare differentially expressed genes between [I (758)](tables/BRCA-I_IIA-I.tsv) and [IIA (448)](tables/BRCA-I_IIA-IIA.tsv).

|I vs IIA: only I|I vs IIA: only IIA|
|:-:|:-:|
|![](images/BRCA-I_IIA-only_I.Reacfoam.jpg)|![](images/BRCA-I_IIA-only_IIA.Reacfoam.jpg)|

> **Remarks**
>
> Stage IIA shows dramatic upregulation of genes involded in cell cycle, responses to stimili and DNA replication and repair. This fits well with the potential metastatic nature of Stage II(A) and cancer progression.

# Mutated BRCA1 gene and amplified ERBB2 in breast cancer (BRCA) -- UCSC XENA Browser
## Forward approach: BRCA1
* Study: TCGA Breast Cancer (BRCA)
* Variable: Genomic - Gene **BRCA1** - *Somatic mutation*
* Variable: Genomic - Gene **ERBB2** - *Copy number*
* Variable: Phenotypic - Show all - select:
  * HER2_Final_Status_nature2012
  * ER_Status_nature2012
  * pathologic_stage
  * ER_Status_nature2012

|BRCA1 KM|ERBB2 KM|
|:-:|:-:|
|![](images/xena-BRCA1-KM.png)|![](images/xena-ERBB2-KM.png)|

> **Remarks**
>
> BRCA1 p-value of KM is not significant, while ERBB2 p-value of KM is *nearly* significant.

|BRCA1 (mutations) vs ERBB2 (copy number) vs Stage vs HER2 vs ER|
|:-:|
|![](images/xena-brca1_erbb2.png)|

> **Remarks**
>
> BRCA1 mutations are found almost exlcusively in samples where ERBB2 number is normal, with the exception of pD366N (erbb2 high), pD1344H (erbb2 high) and pS1139S (erbb2 low). The majority of mutations are from samples at Stage IIA. Two mutations (p.T688Vfs*12, p.D1344H) are found in the HER2-positive samples, also found in ER-positive samples. Five more mutations are found in the ER-positive samples, only. Therefore, the two overlapping mutation may have a connection to both. There are no mutations in the equivocal (HER2) and intermediate (ER).

```mermaid
pie showData
title Mutations per stage
"I" :	2
"IA" : 1
"IIA" : 13
"IIB" : 6
"IIIC" : 1
```

|ERBB2 (copy number) vs Stage vs HER2 vs ER vs BRCA1 (mutations)|ER vs ERBB2|
|:-:|:-:|
|![](images/xena-erbb2-brca1.png)|![](images/xena-er-erbb2.png)|

> **Remarks**
>
> Elevated copy numbers of ERBB2 overlap with HER2-positive samples (naturally, since HER2 is just the old name of ERBB2). More than half of the ERBB2 high copy number samples are also ER-positive. Therefore, a link between ERBB2 copy number and ER may exist.
