# Mutated KRAS gene vs amplified TERT in lung adenocarcinoma (LUAD) -- cBioPortal

## Forward approach: KRAS/TERT
* lung adenocarcinoma (LUAD), [TCGA, Nature 2014](https://www.ncbi.nlm.nih.gov/pubmed/25079552)
* Group A: Mutated genes - KRAS (#Mut: 76, #Samples: 75, Freq: 32.6%)
* Group B: CNA genes - TERT (#Samples: 41, Freq: 17.8%)

### Compare groups - Clinical
* **No significant results found**

## Reverse approach: Summary - Charts - Clinical
### Tumor Stage 2009
* ID: TUMOR_STAGE_2009
* cBioPortal
```mermaid
%%{init: {'theme': 'base' } }%%
pie showData
title Tumor Stage
"IB" :	68
"IA" :  46
"IIIA" : 38
"IIB" : 29
"IIA" : 18
"NA" : 12
"IIIB" : 9
"IV" : 9
"I" : 1
```
* [Stages chart:](https://www.lungevity.org/for-patients-caregivers/navigating-your-diagnosis/lung-cancer-staging)

```mermaid
flowchart LR;

%%This insanity needs some colors...
style 0 fill:#FFF
style I fill:#FCF4E4
style IA fill:#FCF4E4
style IB fill:#FCF4E4

style II fill:#D2E0FC
style IIA fill:#D2E0FC
style IIB fill:#D2E0FC
style IIB5 fill:#D2E0FC
style IIB7 fill:#D2E0FC

style III fill:#F4E4FC
style IIIA fill:#F4E4FC
style IIIB fill:#F4E4FC
style IIIA5 fill:#F4E4FC
style IIIA5+ fill:#F4E4FC
style IIIA7 fill:#F4E4FC
style IIIB5 fill:#F4E4FC
style IIIB5+ fill:#F4E4FC
style IIIC fill:#F4E4FC

style IV fill:#FCC0D3
style IVA fill:#FCC0D3
style IVB fill:#FCC0D3

%% define major nodes
0(stage 0) --> I(stage I) ---> II(stage II) ---> III(stage III) --> IV(stage IV)

I --- IA(IA <3cm)
I --- IB(IB 3-4cm)
scbl(swelling, collapse & bronchus, lining)
IB -...-|may| scbl

II --- IIA(IIA 4-5cm) & IIB
IIA -.-|may| scbl
IIB --- IIB5(IIB <5cm)
IIB --- IIB7(IIB 5-7cm)
IIB5 -..- phi(peribronchial, hilar, intrapulmonary) -.- ln(lymph nodes)
IIB7 -..- cphl(chest wall, phrenic nerve, heart's lining)
IIB7 -..- ps(primary & secondary) -.- tumors

III --- IIIA & IIIB & IIIC(IIIC>5cm)
IIIA --- IIIA5(IIIA <5cm) & IIIA5+(IIIA >5cm) & IIIA7(IIIA >7cm)
IIIB --- IIIB5(IIIB <5cm) & IIIB5+(IIIB >5cm)
IIIA5+ -..- phi(peribronchial, hilar, intrapulmonary) -.- ln
IIIA7 -..-|may| list1(diaphragm, mediastinum, heart, windpipe, laryngeal nerve, carina, esophagus, spine)
IIIA5 -..- mediastinal -.- ln
IIIA5 -..-|may| scbl
IIIB5+ -..- mediastinal -.- ln
IIIB5+ -..- cphl & ps & list1
IIIB5 -..- mediastinal -.- ln
IIIB5 -..- supraclavicular/scalene -.- ln
IIIB5 -..- scbl
IIIC -..- mediastinal & supraclavicular/scalene -.- ln
IIIC -..- cphl & ps & list1

IV --- IVA & IVB
IVA -..- list2(other lung, lining, heart lining, fluid, outside chest)
IVA -.- |may| ln
IVB -.- |may| ln
IVB -.- |spread| outside(outside the chest area)
```
* Differentially expressed genes between stages

|[IA and IB](tables/LUAD-stage-IA_IB-mRNA.tsv)|[IA and IIA](tables/LUAD-stage-IA_IIA-mRNA.tsv)|[IA and IIIA](tables/LUAD-stage-IA_IIIA-mRNA.tsv)|[IA and IIIB](tables/LUAD-stage-IA_IIIB-mRNA.tsv)|[IA and IV](tables/LUAD-stage-IA_IV-mRNA.tsv)|
|:-:|:-:|:-:|:-:|:-:|
|![IA_IB](images/LUAD-stage-IA_IB-mRNA.png)|![IA_IIA](images/LUAD-stage-IA_IIA-mRNA.png)|![IA_IIA](images/LUAD-stage-IA_IIIA-mRNA.png)|![IA_IIA](images/LUAD-stage-IA_IIIB-mRNA.png)|![IA_IV](images/LUAD-stage-IA_IV-mRNA.png)|

|[IB and IIIB](tables/LUAD-stage-IB_IIIB-mRNA.tsv)|[IB and IV](tables/LUAD-stage-IB_IV-mRNA.tsv)|[IIB and IIIB](tables/LUAD-stage-IIB_IIIB-mRNA.tsv)|[IIB and IV](tables/LUAD-stage-IIB_IV-mRNA.tsv)|[IIIA and IV](tables/LUAD-stage-IIIA_IV-mRNA.tsv)|
|:-:|:-:|:-:|:-:|:-:|
|![IB_IIIB](images/LUAD-stage-IB_IIIB-mRNA.png)|![IB_IV](images/LUAD-stage-IB_IV-mRNA.png)|![IIB_IIIB](images/LUAD-stage-IIB_IIIB-mRNA.png)|![IIB_IV](images/LUAD-stage-IIB_IV-mRNA.png)|![IIIA_IV](images/LUAD-stage-IIIA_IV-mRNA.png)|

* Number of differentially expressed genes per stages pair
 (separated by stage  [LUAD-stages-mRNA.csv](tables/LUAD-stages-mRNA.csv)). Dot shows the stage
 to which the indicated number of genes corresponds:

```mermaid
flowchart TB;
style IA fill:#FCF4E4
style IIIA fill:#F4E4FC
IA o-.- |1| IB
IA -.-o |1| IIA -.-o |1| IA
IA -.-o |120| IIIA -.-o |228| IA
IA -.- |5| IIIB -.-o |56|IA
IA o-.- |5| IV
```
```mermaid
flowchart TB;
IB -.-o |2| IIIB -.-o |27| IB
IB -.-o |3| IV -.-o |10| IB
```
```mermaid
flowchart TB;
IIB -.-o |1| IIIB
IIB o-.- |2| IV
```
```mermaid
flowchart TB;
IIIA -.-o |2| IV -.-o |5| IIIA
```

* Compare groups:
  * **FIRST ROW**: Primary Tumor Pathologic Spread (PTPS) (p-value < 0.05)
  * **SECOND ROW**: Distant Metastasis Pathologic Spread (DMPS) (p-value < 0.05)

|IA_IB|IA_IIA|IA_IIIA|IA_IIIB|IA_IV|IB_IIIB|IB_IV|IIB_IIIB|IIB_IV|IIIA_IV|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|![IA_IB](images/LUAD-stage-IA_IB-clinical-PTPS.png)|![IA_IIA](images/LUAD-stage-IA_IIA-clinical-PTPS.png)|![IA_IIIA](images/LUAD-stage-IA_IIIA-clinical-PTPS.png)|![IA_IIIB](images/LUAD-stage-IA_IIIB-clinical-PTPS.png)|![IA_IV](images/LUAD-stage-IA_IV-clinical-PTPS.png)|![IB_IIIB](images/LUAD-stage-IB_IIIB-clinical-PTPS.png)|![IB_IV](images/LUAD-stage-IB_IV-clinical-PTPS.png)|![IIB_IIIB](images/LUAD-stage-IIB_IIIB-clinical-PTPS.png)|![IIB_IV](images/LUAD-stage-IIB_IV-clinical-PTPS.png)||
|||![IA_IIIA](images/LUAD-stage-IA_IIIA-clinical-DMPS.png)||||![IB_IV](images/LUAD-stage-IB_IV-clinical-DMPS.png)||![IIB_IV](images/LUAD-stage-IIB_IV-clinical-DMPS.png)|![IIIA_IV](images/LUAD-stage-IIIA_IV-clinical-DPMS.png)|

> **Remark**
>
> The highest number of genes that are differentially expressed are between IA and IIIA.
> Also, IA vs IIIA show a (significant) difference for **both** PTPS and DMPS.
> Therefore, focus on the genes differentially expressed between IA / IIIA.

* **IA vs IIIA: key differences.** At IA, no spread has yet started, while at IIIA it has. 

```mermaid
flowchart LR;
style IA fill:#FCF4E4
style IIIA fill:#F4E4FC

I(stage I) --> II(stage II) --> III(stage III)
I --- IA(IA <3cm) --- no(not yel spread)
III --- IIIA
IIIA --- IIIA5(IIIA <5cm) & IIIA5+(IIIA >5cm) & IIIA7(IIIA >7cm)
IIIA5+ -..- phi(peribronchial, hilar, intrapulmonary)
IIIA7 -..-|may| list1(diaphragm, mediastinum, heart, windpipe, laryngeal nerve, carina, esophagus, spine)
IIIA5 -..- mediastinal
IIIA5 -..-|may| scbl(chest wall, phrenic nerve, heart's lining)
```
* **Reactome**: genes differentially expressed (clickable) in AI and AIII ([LUAD-stages-mRNA.csv](tables/LUAD-stages-mRNA.csv)):

|IA|IIIA|
|:-:|:-:|
|![AI](images/IA_IIIA-only_IA.Reacfoam.jpg)|![AIII](images/IA_IIIA-only_IIIA.Reacfoam.jpg)|

> **Remark**
>
> The genes from IA are involved mainly in homeostasis and metabolism, but also WNT signalling.
> In contrast, the genes from IIIA are mainly involved in **cell cycle** and **glucose metabolism**, to a lesser extend in
> Rho signalling, which suggests processes of cell proliferation and fits well with the metastasis nature of Stage IIIA.  
>
> **TODO** (eventually):
>
> Do the same for each pair of stages, however, such detailed study would
> take quite some time...

# Mutated KRAS gene vs amplified TERT in lung adenocarcinoma (LUAD) -- UCSC XENA Browser
## Forward approach: KRAS

* Study: TCGA Lung Adenocarcinoma (LUAD)
* First variable: Genomic - Gene **KRAS** - *Somatic mutation*
* Second variable: Genomic - Phenotypic - Show all - select:
  * histological_type
  * gender
  * pathologic_stage
* All parameters:

|zoom on [somatic_mutations](tables/xena-somatic_mutation-kras.tsv)|Kaplan Meier plot|
|:-:|:-:|
|![all](images/xena-kras.png)|![KM](images/xena-kras-km.png)|

* Parameters in pairs (mutations vs type/stage/gender):

|[histological_type](tables/xena-histological_type-kras.tsv)|[pathologic_stage](tables/xena-pathologic_stage-kras.tsv)|[gender](images/xena-kras-gender.png)|
|:-:|:-:|:-:|
|![ht](images/xena-kras-histological_type.png)|![ps](images/xena-kras-pathologic_stage.png)|![g](images/xena-kras-gender.png)|

## Forward approach: TERT
* Study: TCGA Lung Adenocarcinoma (LUAD)
* First variable: Genomic - Gene **TERT** - *Copy number*
* Second variable: Genomic - Phenotypic - Show all - select:
  * histological_type
  * gender
  * pathologic_stage
* All parameters:

|zoom on [copy_number](tables/xena-copy_number-tert.tsv)|Kaplan Meier plot|
|:-:|:-:|
|![all](images/xena-tert.png)|![KM](images/xena-tert-km.png)|

* Parameters in pairs (copy number vs type/stage/gender):

|[histological_type](tables/xena-histological_type-tert.tsv)|[pathologic_stage](tables/xena-pathologic_stage-tert.tsv)|[gender](images/xena-tert-gender.png)|
|:-:|:-:|:-:|
|![ht](images/xena-tert-histological_type.png)|![ps](images/xena-tert-pathologic_stage.png)|![g](images/xena-tert-gender.png)|

> **Remarks**
>
> KRAS mutations are found predominantly from samples from Stage IA, while they seem to be equally distributed between male and female patients. From the graph, it is quite difficult to interpret the connection between each point mutation and the other parameters, such as histological type. TERT amplification is found mostly in samples at Stages IIA and IB, suggesting an involvement in the earlier stages, however it is very difficult to say only from the chart.

# Extra...

* For further analyses, as a start I would sort or divide the data (e.g. KRAS), by a simple BASH script (something like this: [kras.sh](extra/kras/kras.sh)).
* First, modify the input a bit, to make it easier to parse (convert TSV to CSV, eliminate extra columns, etc):
  * [histological_type.csv](extra/kras/histological_type.csv)
  * [somatic_mutation.csv](extra/kras/somatic_mutation.csv)
  * [gender.tsv](extra/kras/gender.tsv)
* `kras.sh` generates:
  * [byType.csv](extra/kras/byType.csv): a spreadsheet summary of the results, where each mutation is plotted against the other parameters
  * [bySite](extra/kras/bySite/): folder containing data sorted by amino acid
* A network can be generated by importing `byType.csv` in Cytoscape, visualizing the connections between each individual mutation and the corresponding histological type (left). Or, edges can be made to distinguish between male and female patients, as well (right, but quite messy).

|Bundled edges (Cytoscape file: [network.cys](extra/kras/network.cys))|Non-bundled edges|
|:-:|:-:|
|![type](extra/kras/byType.csv.png)|![type1](extra/kras/byType1.csv.png)|

* The histological types where most mutations was found were:
  * Lung Adenocarcinoma Not Otherwise Specified (NOC)
  * Lung Adenocarcinoma Mixed subtype (8)
  * Lung Pappilary Adenocarcinoma (7)
* The amino acids found in most histological subtypes were:
  * p.G12V (9)
  * p.G12C (7)
* To get some actual numbers, `datamash` is very nice. Navigate to the folder where information of each mutation was collected (`bySite`) and do:
  ```
  cd bySite
  for i in * ; do echo -e "${i%.*},\n `cat $i | datamash --field-separator=, -sg 1 count 1`\n" >> ../bySite.csv ; done
  ```
  * This will output data in a single file [bySite.csv](extra/kras/bySite.csv). Change the column to be counted from the input files, e.g. `-sg 1 count 1` counts the histological types, while `-sg 2 count 2` will count genders.
  * Simple chart...:

```mermaid
pie showData
%%{init: {'theme': 'base' } }%%
title p.G12C	
"Acinar Adenocarcinoma" :	1
"Adenocarcinoma Mixed Subtype" :	13
"Adenocarcinoma- Not Otherwise Specified (NOS)" :	35
"Bronchioloalveolar Carcinoma Nonmucinous" :	3
"Clear Cell Adenocarcinoma" :	1
"Papillary Adenocarcinoma" :	3
"Mucinous (Colloid) Carcinoma" :	5
```
