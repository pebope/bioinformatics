# Exercise 1: BRCA1/ERBB2 and MUC4/MYC in breast cancer (BRCA)

## Step 1 - BRCA1/ERBB2 in cBioPortal
* breast cancer (BRCA), [TCGA, Cell 2015](https://www.ncbi.nlm.nih.gov/pubmed/26451490)
* Group A: Mutated genes - BRCA1 (#Mut: 18, Freq: 2.2%)
* Group B: CNA genes - ERBB2 (#Samples: 105, Freq: 12.9%)

| **overlap** |
|---|
| ![overlap](images/overlap-BRCA1_ERBB2.png) |

| **mRNA** |
|---| 
| ![mRNA](images/enrichments-volcano-BRCA1_ERBB2.png) |
* Genes (# 191) [Table 1](tables/table_1.tsv) with high expression in Groups (A) and (B); Significant only (q-value < 0.05)

## Step 2 - MUC4/MYC in cBioPortal
* breast cancer (BRCA), [TCGA, Cell 2015](https://www.ncbi.nlm.nih.gov/pubmed/26451490)
* Group A: Mutated genes - MUC (#Mut: 60, Freq: 6.1%)
* Group B: CNA genes - MYC (#Samples: 173, Freq: 21.2)

| **overlap** |
|---|
| ![overlap](images/overlap-MUC4_MYC.png) |

| **mRNA** |
|---| 
| ![mRNA](images/enrichments-volcano-MUC4_MYC.png) |
* Genes (# 56) [Table 2](tables/table_2.tsv) with high expression in Groups (A) and (B); Significant only (q-value < 0.05)

## Step 3 - Enrichments and networks
* [DeepVenn](http://www.deepvenn.com/): no overlapping genes between the two datasets:

| **BRCA1/ERBB2 analysis vs MUC4/MYC dataset** |
|:---:|
| ![deepvenn](images/deepvenn.png) |


* STRING

| **Genes from [Table 1](tables/table_1.tsv) (BRCA1/ERBB2 analysis)**; [network (tsv)](tables/table_3-string_interactions_short.tsv); |
|---|
|number of nodes:	174; number of edges:	137; average node degree:	1.57; avg. local clustering coefficient:	0.306; PPI enrichment p-value:	9.42e-08|
| ![overlap](images/string_normal_image-BRCA1_ERBB2.png) |

| **Genes from [Table 2](tables/table_2.tsv) (MUC4/MYC analysis)**; [network (tsv)](tables/table_4-string_interactions_short.tsv); |
|---|
|number of nodes:	54; number of edges:	54; average node degree:	2; avg. local clustering coefficient:	0.356; PPI enrichment p-value:	< 1.0e-16|
| ![overlap](images/string_normal_image-MUC4_MYC.png) |

* REACTOME

| **Genes from [Table 1](tables/table_1.tsv) (BRCA1/ERBB2 analysis); [top 25](tables/table_5-reactome-BRCA1_ERBB2.csv)** |
|---|
| ![overlap](images/Reacfoam-BRCA1_ERBB2.jpg) |

| **Genes from [Table 2](tables/table_2.tsv) (MUC4/MYC analysis); [top 25](tables/table_6-reactome-MUC4_MYC.csv)** |
|---|
| ![overlap](images/Reacfoam-MUC4_MYC.jpg) |

## Analysis
* BRCA1/ERBB2 associated genes
  * STRING; major hub proteins (> 10 edges): PGAP3, ERBB2, STARD3
    * PGAP3: Post-GPI attachment to proteins factor 3; Involved in the lipid remodeling steps of **GPI-anchor maturation**. Lipid remodeling steps consist in the generation of 2 saturated fatty chains at the sn-2 position of GPI-anchors proteins. Required for phospholipase A2 activity that removes an acyl-chain at the sn-2 position of GPI-anchors during the remodeling of GPI (Probable); Belongs to the PGAP3 family
    * ERBB2: Receptor tyrosine-protein kinase erbB-2; Protein tyrosine kinase that is part of several cell surface receptor complexes, but that apparently needs a coreceptor for ligand binding. Essential component of a neuregulin-receptor complex, although neuregulins do not interact with it alone. GP30 is a potential ligand for this receptor. Regulates outgrowth and stabilization of peripheral microtubules (MTs). Upon ERBB2 activation, the MEMO1-RHOA-DIAPH1 signaling pathway elicits the phosphorylation and thus the **inhibition of GSK3B** at cell membrane. This prevents the phosphorylation of APC and [...]
    * STARD3: StAR-related lipid transfer protein 3; Sterol-binding protein that mediates **cholesterol transport from the endoplasmic reticulum to endosomes**. Creates contact site between the endoplasmic reticulum and late endosomes: localizes to late endosome membranes and contacts the endoplasmic reticulum via interaction with VAPA and VAPB. Acts as a lipid transfer protein that redirects sterol to the endosome at the expense of the cell membrane and favors membrane formation inside endosomes. May also mediate cholesterol transport between other membranes, such as mitochondria membrane or cell membr [...]
  * REACTOME pathways (top 10 by p-value)
    1. Insulin-like Growth Factor-2 mRNA Binding Proteins (IGF2BPs/IMPs/VICKZs) bind RNA
    2. Sphingolipid de novo biosynthesis
    3. G1/S-Specific Transcription
    4. Circadian Clock
    5. Aspirin ADME
    6. RUNX3 regulates RUNX1-mediated transcription
    7. GRB7 events in ERBB2 signaling
    8. TLR3-mediated TICAM1-dependent programmed cell death
    9. RORA activates gene expression
    10. Androgen biosynthesis
  * **Conclusion: associated genes are implicated in breast cancer via metabolism, protein maturation and gene expression.**

* MUC4/MYC associated genes
  * STRING; major hub proteins (> 5 edges): DCAF13, RAD21
    * DCAF12: DDB1- and CUL4-associated factor 13; Possible role in **ribosomal RNA processing** (By similarity). May function as a substrate receptor for CUL4-DDB1 E3 ubiquitin-protein ligase complex; DDB1 and CUL4 associated factors
    * RAD21: Double-strand-break repair protein rad21 homolog; Cleavable component of the cohesin complex, involved in chromosome cohesion during cell cycle, in **DNA repair**, and in **apoptosis**. The cohesin complex is required for the cohesion of sister chromatids after DNA replication. The cohesin complex apparently forms a large proteinaceous ring within which sister chromatids can be trapped. At metaphase-anaphase transition, this protein is cleaved by separase/ESPL1 and dissociates from chromatin, allowing sister chromatids to segregate. The cohesin complex may also play a role in spindle pole asse [...]
  * REACTOME (top 10 by p-value)
    1. Estrogen-dependent gene expression
    2. ESR-mediated signaling
    3. Signaling by Nuclear Receptors
    4. TP53 regulates transcription of several additional cell death genes whose specific roles in p53-dependent apoptosis remain uncertain
    5. Metalloprotease DUBs
    6. DNA methylation
    7. HATs acetylate histones
    8. Apoptotic cleavage of cellular proteins
    9. PRC2 methylates histones and DNA
    10. RNA Polymerase II Transcription Pre-Initiation And Promoter Opening
  * **Conclusion: associated genes are implicated in breast cancer via signa transduction, gene expression (RNA polymerase and epigenetic regulation), apoptosis and to a lesser extend protein metablism, such as SUMOylation.**
