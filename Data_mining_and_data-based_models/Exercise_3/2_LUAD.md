# Exercise 2: KRAS/TERT and MUC16/SOX2 in lung adenocarcinoma (LUAD) and squamous lung carcinoma (LUSC), respectively

## Step 1 - KRAS/TERT in cBioPortal
* lung adenocarcinoma (LUAD), [TCGA, Nature 2015](https://www.ncbi.nlm.nih.gov/pubmed/25079552)
* Group A: Mutated genes - KRAS (#Mut: 76, #Samples: 75, Freq: 32.6%)
* Group B: CNA genes - TERT (#Samples: 41, Freq: 17.8%)

| **overlap** |
|---|
| ![overlap](images/overlap-KRAS_TERT.png) |

| **mRNA** |
|---| 
| ![mRNA](images/enrichments-volcano-KRAS_TERT.png) |
* Genes (# 240) [Table KRAS and TERT](tables/table_5.tsv) with high expression in Groups (A) and (B); Significant only (q-value < 0.05)

## Step 2 - MUC16/SOX2 in cBioPortal
* squamous lung carcinoma (LUSC), [TCGA, Firehose legacy](https://gdac.broadinstitute.org/runs/stddata__2016_01_28/data/LUAD/20160128/)
* Group A: Mutated genes - MUC16 (#Mut: 161, #Samples: 91, Freq: 39.6%)
* Group B: CNA genes - SOX2 (#Samples: 14, Freq: 2.7%)

| **overlap** |
|---|
| ![overlap](images/overlap-MUC16_SOX2.png) |

| **mRNA** |
|---| 
| ![mRNA](images/enrichments-volcano-MUC16_SOX2.png) |
* Genes (# 85) [Table MUC16 and SOX2](tables/table_6.tsv) with high expression in Groups (A) and (B); Significant only (q-value < 0.05)

## Step 3 - Enrichments and networks
* [DeepVenn](http://www.deepvenn.com/): 1 overlapping gene (PLEKHH1) between the two datasets:

| **KRAS/TERT (A) vs MUC16/SOX2 (B)** |
|:---:|
| ![deepvenn](images/deepvenn2.png) |


* STRING

| **Genes from [Table KRAS and TERT](tables/table_5.tsv)**; [network (tsv)](tables/table_5-string_interactions_short.tsv); |
|---|
|number of nodes:	233; number of edges:	179; average node degree:	1.54; avg. local clustering coefficient:	0.409; PPI enrichment p-value:	0.0051 |
| ![overlap](images/string_normal_image-KRAS_TERT.png) |

| **Genes from [Table MUC16 and SOX2](tables/table_6.tsv)**; [network (tsv)](tables/table_6-string_interactions_short.tsv); |
|---|
|number of nodes:	70; number of edges:	10; average node degree:	0.286; avg. local clustering coefficient:	0.229; PPI enrichment p-value:	0.328 ; network does **not** have significantly more interactions than expected |
| ![overlap](images/string_normal_image-MUC16_SOX2.png) |

* REACTOME

| **Genes from [Table KRAS and TERT](tables/table_5.tsv); [top 25, p-value < 0.05](tables/table_7-reactome-KRAS_TERT.csv)** |
|---|
| ![overlap](images/Reacfoam-KRAS_TERT.jpg) |

| **Genes from [Table MUC16 and SOX2](tables/table_6.tsv); [p-value < 0.05](tables/table_8-reactome-MUC16_SOX2.csv)** |
|---|
| ![overlap](images/Reacfoam-MUC16_SOX2.jpg) |

## Analysis
* KRAS7TERT associated genes
  * STRING; major hub proteins (top 3): EEF2 (11), RPA3 (7), SDHA (7)
    * EEF2: Elongation factor 2; Catalyzes the GTP-dependent ribosomal translocation step during **translation elongation**. During this step, the ribosome changes from the pre-translocational (PRE) to the post- translocational (POST) state as the newly formed A-site-bound peptidyl-tRNA and P-site-bound deacylated tRNA move to the P and E sites, respectively. Catalyzes the coordinated movement of the two tRNA molecules, the mRNA and conformational changes in the ribosome; Belongs to the TRAFAC class translation factor GTPase superfamily. Classic translation factor GTPase family. EF-G/EF-2 subfamily
    * RPA3: Replication protein A 32 kDa subunit; As part of the heterotrimeric replication protein A complex (RPA/RP-A), binds and stabilizes single-stranded DNA intermediates, that form during **DNA replication** or upon DNA stress. It prevents their reannealing and in parallel, recruits and activates different proteins and complexes involved in DNA metabolism. Thereby, it plays an essential role both in **DNA replication** and the cellular **response to DNA damage**. In the cellular response to DNA damage, the RPA complex controls **DNA repair** and **DNA damage checkpoint activation**. Through recruitment of ATRI [...]
    * SDHA2: Succinate dehydrogenase [ubiquinone] flavoprotein subunit, mitochondrial; Flavoprotein (FP) subunit of succinate dehydrogenase (SDH) that is involved in complex II of the mitochondrial **electron transport chain** and is responsible for transferring electrons from succinate to ubiquinone (coenzyme Q). Can act as a **tumor suppressor**; Belongs to the FAD-dependent oxidoreductase 2 family. FRD/SDH subfamily
  * REACTOME pathways (top 5 by p-value)
    1. FOXO-mediated transcription of **oxidative stress**, **metabolic** and neuronal genes
    2. Calcitonin-like ligand **receptors**
    3. Processing and activation of **SUMO**
    4. Rhesus blood group biosynthesis
    5. **RAC1** GTPase cycle
    6. TP53 Regulates Transcription of **Death Receptors and Ligands**
    7. Activation of the **pre-replicative complex**
    8. **SUMO** is proteolytically processed
    9. Activation of ATR in response to **replication stress**
    10. Diseases of **DNA Double-Strand Break Repair**
  * **Conclusion: associated genes are implicated in oxidative stress, translation, DNA replication and repair, as well as signalling and protein degradation.**

* MUC16/SOX2 associated genes
  * STRING; major hub proteins (top 2): ECT2, MSH5
    * ECT2: Protein ECT2; Guanine nucleotide exchange factor (GEF) that catalyzes the exchange of GDP for GTP. Promotes guanine nucleotide exchange on the Rho family members of small GTPases, like RHOA, RHOC, RAC1 and CDC42. Required for **signal transduction** pathways involved in the regulation of **cytokinesis**. Component of the centralspindlin complex that serves as a microtubule-dependent and Rho-mediated signaling required for the myosin contractile ring formation during the **cell cycle** cytokinesis. Regulates the translocation of RHOA from the **central spindle** to the equatorial region. Plays a role i [...]
    * MSH5: MutS protein homolog 5; Involved in **DNA mismatch repair** and **meiotic recombination** processes. Facilitates crossovers between homologs during **meiosis** (By similarity); Belongs to the **DNA mismatch repair** MutS family
  * REACTOME (top by p-value)
    1. Depolymerisation of the **Nuclear Lamina**
    2. IRF3 mediated activation of **type 1 IFN**
  * **Conclusion: associated genes are implicated in signalling, cell cycle and DNA reparative mechanisms.**
