# (f)ilename to open (str)
def show_results(f):
    songs = []
    with open(f) as contents:
        for row in contents.readlines():
            songs.append(row.strip("\n").split(","))
        for s in songs:
            print("{} {} - {} {}".format(s[0], s[2], s[3], s[1]))

show_results("hemulencup.csv")
