room = [['N', ' ', ' ', ' ', ' '],
        ['N', 'N', 'N', 'N', ' '],
        ['N', ' ', 'N', ' ', ' '],
        ['N', 'N', 'N', ' ', ' '],
        [' ', ' ', ' ', ' ', ' '],
        [' ', ' ', ' ', ' ', ' ']]

def count_ninjas(x, y, room):
    """
    Counts the ninjas surrounding one tile in the given room and
    returns the result. The function assumes the selected tile does
    not have a ninja in it - if it does, it counts that one as well.
    """
    
    ninjas = []
    
    neighbours = {
        "ul": (y - 1, x - 1),
        "up": (y - 1, x),
        "ur": (y - 1, x + 1),
        "le": (y, x - 1),
        "ri": (y, x + 1),
        "dl": (y + 1, x - 1),
        "do": (y + 1, x),
        "dr": (y + 1, x + 1)
    }
    
    for row in enumerate(room):
        row_i, row_c = row
        for cell in enumerate(row_c):
            cell_i, cell_c = cell
            for n in neighbours:
                ninja_y, ninja_x = neighbours[n]
                if ninja_y == row_i and ninja_x == cell_i and cell_c == "N":
                    ninjas.append("N")
    #print(ninjas.count("N"))
    return ninjas.count("N")

try:
    x = int(input("Enter X: "))
    y = int(input("Enter Y: "))
except ValueError:
    print("Enter integer")
else:
    count_ninjas(x, y, room)
