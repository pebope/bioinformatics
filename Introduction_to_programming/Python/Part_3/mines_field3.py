ANIMALS = {
    "s": "sheep",
    "d": "doggo",
    "@": "cat",
    "m": "moogle",
    "c": "chocobo"
}

field = [
    [" ", "s", " ", " ", "m"],
    [" ", "d", "@", "d", " "],
    ["c", " ", "s", "d", " "]
]

def explore_tile(a, y, x):
    """
    Explore a tile - if there is an animal, prints the
    location and name of the animal
    """
    if a in ANIMALS:
        print(f"Tile ({x}, {y}) contains {ANIMALS[a]}")

def explore_field(fld):
    """
    This function explores an entire field by calling the explore_tile
    function for each tile in the field.
    """
    for row in enumerate(fld):
        row_i, row_c = row
        for tile in enumerate(row_c):
            tile_i, tile_c = tile
            if not tile_c == " ":
                explore_tile(tile_c, row_i, tile_i)


explore_field(field)
