def prompt_password():
    password = str(input("Write password: "))
    while True:
        if len(password) < 8:
            print("The password must be at least 8 characters long")
            password = str(input("Write password: "))    
        else:
            return password

print(prompt_password())
