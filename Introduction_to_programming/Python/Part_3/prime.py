def prompt_input(ask, answer):
    """
    Prompts the user for an integer using the prompt parameter.
    If an invalid input is given, an error message is shown using
    the error message parameter. A valid input is returned as an
    integer. Only accepts integers that are bigger than 1.
    """
    
    while True:
        try:
            entered=int(input(ask))
        except ValueError:
            print(answer)
        else:
            if entered > 1:
                return entered
            else:
                print(answer)

number = prompt_input(
    "Give an integer that's bigger than 1: ",
    "You had one job"
)

def check_prime(num):
    """
    Checks whether an integer is a prime number. Returns False
    if the number isn't a prime; if it is a prime, returns True
    https://www.pythonpool.com/check-if-number-is-prime-in-python/
    """
    for n in range(2,int(num**0.5)+1):
        if num%n==0:
            return False
    return True

if check_prime(number):
    print("This is a prime")
else:
    print("This is not a prime")
