room = [['N', ' ', ' ', ' ', ' '],
        ['N', 'N', 'N', 'N', ' '],
        ['N', ' ', 'N', ' ', ' '],
        ['N', 'N', 'N', ' ', ' '],
        [' ', ' ', ' ', ' ', ' '],
        [' ', ' ', ' ', ' ', ' ']]

# i: position, c: contents
y_len = (len(room))
x_len = (len(room[0]))
#print(y_len)
#print(x_len)

y, x = 1, 1

neighbours = {
    "ul": (y - 1, x - 1),
    "up": (y - 1, x),
    "ur": (y - 1, x + 1),
    "le": (y, x - 1),
    "ri": (y, x + 1),
    "dl": (y + 1, x - 1),
    "do": (y + 1, x),
    "dr": (y + 1, x + 1)
}

# x, y: tile coordinated; r: room;
# _i: index; _c: contents
def pre_count_ninjas(y, x, r):
    for row in enumerate(r):
        row_i, row_c = row
        for tile in enumerate(row_c):
            tile_i, tile_c = tile
            if row_i == y and tile_i == x:
                print("N")
                return "N"

def count_ninjas(x, y, r):
    ninjas = []
    for n in neighbours:
        nghb_y, nghb_x = neighbours[n]
        if 0 <= nghb_x <= x_len and 0 <= nghb_y <= y_len:
            #print(nghb_y, nghb_x)
            ninjas.append(pre_count_ninjas(nghb_y, nghb_x, room))
    
    print(ninjas.count("N"))

count_ninjas(x, y, room)
