from turtle import *

def draw_spiral(c, arks, radius, growth, weight=1):
    pensize(weight)
    color(c)
    for i in range(arks):
        radius += growth
        down()
        circle(radius, 90)

draw_spiral("black", 20, 10, 3)
draw_spiral("red", 10, 20, 4, 3)
draw_spiral("blue", 10, -20, -4, 3)
done()
