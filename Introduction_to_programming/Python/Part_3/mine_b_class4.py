try:
    y = int(input("Enter Y: "))
    x = int(input("Enter X: "))
except ValueError:
    print("Enter integer")
    exit()

#y, x = 1, 1

room = [['N', ' ', ' ', ' ', ' '],
        ['N', 'N', 'N', 'N', ' '],
        ['N', ' ', 'N', ' ', ' '],
        ['N', 'N', 'N', ' ', ' '],
        [' ', ' ', ' ', ' ', ' '],
        [' ', ' ', ' ', ' ', ' ']]

neighbours = {
    "ul": (y - 1, x - 1),
    "up": (y - 1, x),
    "ur": (y - 1, x + 1),
    "le": (y, x - 1),
    "ri": (y, x + 1),
    "dl": (y + 1, x - 1),
    "do": (y + 1, x),
    "dr": (y + 1, x + 1)
}

y_len = (len(room))
x_len = (len(room[0]))

#for n in neighbours:
#    ninja_y, ninja_x = neighbours[n]
#    print(ninja_y, ninja_x)

ninjas = []
for row in enumerate(room):
    row_i, row_c = row
    #print(row_i, row_c)
    for cell in enumerate(row_c):
        cell_i, cell_c = cell
        #print(cell_i, cell_c)
        for n in neighbours:
            ninja_y, ninja_x = neighbours[n]
            if ninja_y == row_i and ninja_x == cell_i and cell_c == "N":
                #print("N")
                ninjas.append("N")
print(ninjas.count("N"))