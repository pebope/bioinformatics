ANIMALS = {
    "s": "sheep",
    "d": "doggo",
    "@": "cat",
    "m": "moogle",
    "c": "chocobo"
}

field = [
    [" ", "s", " ", " ", "m"],
    [" ", "d", "@", "d", " "],
    ["c", " ", "s", "d", " "]
]

for row in enumerate(field):
    row_i, row_c = row
    for tile in enumerate(row_c):
        tile_i, tile_c = tile
        if not tile_c == " ":
            an = ANIMALS[tile_c]
            print("Tile ({}, {}) contains {}".format(tile_i, row_i, an))
