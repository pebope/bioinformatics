ANIMALS = {
    "s": "sheep",
    "d": "doggo",
    "@": "cat",
    "m": "moogle",
    "c": "chocobo"
}

field = [
    [" ", "s", " ", " ", "m"],
    [" ", "d", "@", "d", " "],
    ["c", " ", "s", "d", " "]
]

def explore_tile(a, y, x):
    """
    Explore a tile - if there is an animal, prints the
    location and name of the animal
    """
    if a in ANIMALS:
        print(f"Tile ({x}, {y}) contains {ANIMALS[a]}")

def explore_field(fld):
    """
    This function explores an entire field by calling the explore_tile
    function for each tile in the field.
    """
    for row in fld:
        row_i = fld.index(row)
        for tile in row:
            tile_i = row.index(tile)
            explore_tile(tile, row_i, tile_i)


explore_field(field)

# for row in field:
#     for tile in row:
#         row_i = field.index(row)
#         tile_i = row.index(tile)
#         if not tile == " ":
#             an = ANIMALS[tile]
#             print("Tile ({}, {}) contains {}".format(tile_i, row_i, an))
