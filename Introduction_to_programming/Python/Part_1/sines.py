import math

def convert_to_xy(angle, ray):
    x = int(float(round(ray * math.cos(angle))))
    y = int(float(round(ray * math.sin(angle))))
    return x, y

angle = float(input("angle as radians: "))
ray = float(input("ray: "))
print(convert_to_xy(angle, ray))
