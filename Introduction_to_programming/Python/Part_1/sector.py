import math

def calculate_sector_area(radius, angle):
    sector = float(angle / 360)
    circle = float(math.pi * radius ** 2)
    sector_area = float(sector * circle)
    return sector_area

radius = float(input("Input circle radius: "))
angle = float(input("Input sector angle: "))
print(round(calculate_sector_area(radius, angle), 4))
