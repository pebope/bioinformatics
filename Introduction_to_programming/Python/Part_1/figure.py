import math

def calculate_square_area(side_length):
    square_area = float(side_length) ** 2
    return square_area

def calculate_sector_area(radius, angle):
    sector = float(angle / 360)
    circle = float(math.pi * radius ** 2)
    sector_area = float(sector * circle)
    return sector_area

def calculate_catheti(hypotenuse):
    catheti_length = float(math.sqrt((hypotenuse ** 2) / 2))
    return catheti_length

def calculate_figure_area(x):
    area = square_area + sector_area + catheti_length
    return area

def calculate_figure_area(x):
    small_square = float(calculate_square_area(x))
    catheti = float(calculate_catheti(x))
    sector = float(calculate_sector_area(catheti, 45))
    triangle = float((catheti ** 2) / 2)
    big_square = float(calculate_square_area(2 * catheti))
    big_circle = float(math.pi * (2 * catheti) ** 2)
    result = float(small_square + sector + triangle + big_square + big_circle - (big_circle / 4))
    return result

x = float(input("Input side length: "))
print(round(calculate_figure_area(x), 4))
