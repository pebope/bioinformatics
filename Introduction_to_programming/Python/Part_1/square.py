import math

def calculate_square_area(side_length):
    square_area = float(side_length) ** 2
    return square_area

side_length = input("Input side length: ")
#side_length = 4.628
print(round(calculate_square_area(side_length), 4))
