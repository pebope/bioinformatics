import math

def calculate_catheti(hypotenuse):
    catheti_length = float(math.sqrt((hypotenuse ** 2) / 2))
    return catheti_length

hypotenuse = float(input("Input the hypotenuse length of a right isosceles triangle: "))
print(round(calculate_catheti(hypotenuse), 4))
