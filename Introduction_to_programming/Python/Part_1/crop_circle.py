from turtle import *

def draw_circle(x, y, radius):
    up() # lifts the pen up so that it can be moved without drawing
    color("blue")
    setx(x)
    sety(y-radius)
    down()
    circle(radius)
    down()

draw_circle(50, 50, 30)
draw_circle(-50, 50, 30)
draw_circle(0, 0, 60)
up()
setx(0)
sety(0)
done()
