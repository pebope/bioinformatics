def length():
    print("Length was selected")

def mass():
    print("Mass was selected")

def volume():
    print("Volume was selected")

def temperature():
    print("Temperature was selected")

print("This program converts US customary units to SI units")
print("Available features:")
print("length")
print("mass")
print("volume")
print("temperature")
print()
choice = input("Make your choice: ")
if choice == "length":
    length()
elif choice == "mass":
    mass()
elif choice == "volume":
    volume()
elif choice == "temperature":
    temperature()
else:
    print("The selected feature is not available")