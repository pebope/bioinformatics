try:
    number_1 = int(input("Input first number: "))
    number_2 = int(input("Input second number: "))
except ValueError:
    print("This doesn't look like a number")
else:
    if number_1 == 0:
        print("{} is a factor of {}.".format(number_2, number_1))
    elif number_2 == 0:
        print("{} is a factor of {}.".format(number_1, number_2))
    elif number_1 % number_2 == 0: 
        print("{} is a factor of {}.".format(number_2, number_1))
    elif number_2 % number_1 == 0:
        print("{} is a factor of {}.".format(number_1, number_2))
    else:   
        print("Neither of the numbers is the other's factor.")
