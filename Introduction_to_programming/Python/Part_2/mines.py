MESSAGES = {
    "outside": "The tile is outside the field.",
    "corner": "The tile is in the corner of the field.",
    "edge": "The tile is on the edge of the field.",
    "middle": "The tile is in the middle of the field."
}

def position_in_field():
    pass

def print_position():
    pass

try:
    width = int(input("Input field width: "))
    height = int(input("Input field height: "))
except ValueError:
    print("Enter a number.")
else:
    if width <= 0 or height <= 0:
        print("You can't fit a single tile on a field that small!")
    else:
        pass

try:
    x = int(input("Input x coordinate: "))
    y = int(input("Input y coordinate: "))
except ValueError:
    print("Enter a number.")
else:
    pass
