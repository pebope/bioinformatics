try:
    integer = int(input("Give an integer: "))
    bits = int(input("Give hexadecimal length (number of bits): "))
except ValueError:
    print("Integer please")
else:
    def format_hex(integer, bits):
        myhex=hex(integer).lstrip("0x")
        digits=int(bits // 4)
        result=myhex.zfill(digits)
        return result

print(format_hex(integer, bits))
