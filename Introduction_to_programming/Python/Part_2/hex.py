try:
    integer = int(input("Give an integer: "))
    bits = int(input("Give hexadecimal length (number of bits): "))
except ValueError:
    print("Integer please")
else:
    def format_hex(integer, bits):
        myhex=hex(integer).lstrip("0x")
        zeros=int(len(hex(bits)))
        if isinstance(myhex, int):
            result=myhex.zfill(zeros)
        else:
            result=myhex
        return result

print(format_hex(integer, bits))
