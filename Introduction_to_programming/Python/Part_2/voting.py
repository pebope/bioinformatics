tax_renewal = {
    "yay": 0,
    "nay": 0,
    "idk": 0,
    "error": 0
}

pooh_for_president = {
    "yay": 12,
    "nay": 0,
    "idk": 5,
    "error": 4
}

def vote(election):
    votes = str(input(": ").lower())
    if (votes == "yay" or votes == "nay" or votes == "idk"):
        election[votes] += 1
    else:
        election["error"] += 1

# Display bars, as "#" repeated a number (n) of times
def display_bars(n):
    bar = ("#" * n)
    return bar

# Show results for a specified dictionary (d)
def show_results(d):
    print("Yay: ", display_bars(d["yay"]))
    print("Nay: ", display_bars(d["nay"]))
    print("Idk: ", display_bars(d["idk"]))
    print("Error: ", display_bars(d["error"]))

print("""Should we implement the tax renewal?
Give your vote, the options are:
yay, nay, idk""")
# Call voting function 5 times
# https://stackoverflow.com/a/9048006
for _ in range(5):
    vote(pooh_for_president)

show_results(pooh_for_president)

print("""Vote Winnie the Pooh for president?
Give your vote, the options are:
yay, nay, idk""")
for _ in range(5):
    vote(tax_renewal)
    
show_results(tax_renewal)
