MESSAGES = {
    "outside": "The tile is outside the field.",
    "corner": "The tile is in the corner of the field.",
    "edge": "The tile is on the edge of the field.",
    "middle": "The tile is in the middle of the field."
}

def position_in_field(x, y, w, h):
    if x == 0 and y == 0:
        return "corner"
    elif x < 0 and y < 0:
        return "outside"
    elif x == 0 and h == (y + 1):
        return "corner"
    elif y == 0 and w == (x + 1):
        return "corner"
    elif w == (x + 1) and h == (y + 1):
        return "corner"
    elif w > (x + 1) and y == 0:
        return "edge"
    elif h > (y + 1) and x == 0:
        return "edge"
    elif w == (x + 1) and h > (y + 1):
        return "edge"
    elif w > (x + 1) and h > (y + 1):
        return "middle"
    elif w == (x + 1) or h == (y + 1):
        return "edge"
    else:
        return "outside"

def print_position(p):
    print(MESSAGES[p])

try:
    width = int(input("Input field width: "))
    height = int(input("Input field height: "))
    if width > 0 and height > 0:
        try:
            x = int(input("Input x coordinate: "))
            y = int(input("Input y coordinate: "))
        except ValueError:
            print("Enter an integer.")
        else:
            #position_in_field(x, y, width, height)
            print_position(position_in_field(x, y, width, height))
    else:
        print("You can't fit a single tile on a field that small!")

except ValueError:
    print("Enter an integer.")
