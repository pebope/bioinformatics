import operator

inop = str(input("Choose operation (+, -, *, /): "))

if (inop == "+" or inop == "-" or inop == "*" or inop == "/"):
    try:
        first = float(input("Give 1st number: "))
        second = float(input("Give 2nd number: "))
    except ValueError:
        print("I don't think this is a number")
    else:
        operator = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": operator.truediv
        }
        op = operator[inop]
        try:
            result=float(op(first, second))
            print("Result: {}".format(result))
        except ZeroDivisionError:
            print("This program can't reach infinity")
else:
    print("Selected operation doesn't exist")
