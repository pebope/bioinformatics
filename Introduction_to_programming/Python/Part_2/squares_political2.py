from turtle import *

def draw_square(side, x, y):
    #int(x)
    #int(y)
    #int(side)
    if x < 0:
        c = "red"
    elif x > 0:
        c = "blue"
    else:
        c = "black"

    hideturtle()
    up()
    setx(x)
    sety(y)
    down()
    color(c)
    begin_fill()
    forward(side)
    right(90)
    forward(side)
    right(90)
    forward(side)
    right(90)
    forward(side)
    end_fill()

draw_square(40, -100, 100)
draw_square(60, 100, -100)
draw_square(100, -50, -20)
draw_square(80, 90, 30)
done()
