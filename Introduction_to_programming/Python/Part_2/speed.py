try:
    meters = float(input("Input distance traveled (m): "))
    seconds = float(input("Input elapse time (s): "))
except ValueError:
    print("You need less donuts, and more number inputting.")
else:
    km = float(meters / 1000)
    h = float(seconds / 60 / 60)
    speed = float(km / h)
    print("The speed of a car traveling {meters:.2f} meters in {seconds:.2f} seconds is {speed:.2f} km/h.".format(meters=meters, seconds=seconds, speed=speed))
