MESSAGES = {
    "outside": "The tile is outside the field.",
    "corner": "The tile is in the corner of the field.",
    "edge": "The tile is on the edge of the field.",
    "middle": "The tile is in the middle of the field."
}

try:
    width = int(input("Input field width: "))
    height = int(input("Input field height: "))
    if width > 0 and height > 0:
        try:
            x = int(input("Input x coordinate: "))
            y = int(input("Input y coordinate: "))
        except ValueError:
            print("Enter an integer.")
        else:
            def position_in_field(width, height, x, y):
                if width < (x + 1) or height < (y + 1):
                    return "outside"
                elif width == (x + 1) and height == (y + 1):
                    return "corner"
                elif x == 0 and y == 0:
                    return "corner"
                elif width == (x + 1) or height == (y + 1):
                    return "edge"
                else:
                    return "middle"

            def print_position(p):
                print(MESSAGES[p])

            position_in_field(width, height, x, y)
            print_position(position_in_field(width, height, x, y))
    else:
        print("You can't fit a single tile on a field that small!")

except ValueError:
    print("Enter an integer.")
